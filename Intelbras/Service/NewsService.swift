//
//  NewsService.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 21/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import Foundation

protocol NewsRequest {
    func fetch(completion: @escaping (Any, Bool) -> Void )
}

class NewsService: NewsRequest {
    func fetch(completion: @escaping (Any, Bool) -> Void) {
        let api = API()
        DispatchQueue.global(qos: .background).async {
            api.get(url: api.url, parameters: nil, success: { (_, response) in
                let itens = self.parseJson(response: response)
                completion(itens, !itens.isEmpty)
            }, failure: { (_, error) in
                completion(error, false)
            })
        }
    }
    
    func parseJson(response: Any) -> [News] {
        guard let data = response as? Data else {return []}
        do {
            let decoder = JSONDecoder()
            decoder.keyDecodingStrategy = .convertFromSnakeCase
            let result: [News] = try decoder.decode([News].self, from: data)
            return result
        } catch let error {
            NSLog("Erro ao converter para JSON: %@", error.localizedDescription)
            return []
        }
    }
}
