//
//  API.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 22/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import Foundation
import Alamofire

class API {
    
    let url = "http://ec2-34-215-199-111.us-west-2.compute.amazonaws.com:5000/desafio"
    let httpClient: HTTPClient = HTTPClient()
    
    func get(url: URLConvertible, parameters: Parameters?, success: @escaping (Int, Any) -> Void,
             failure: @escaping (Int, Any) -> Void) {
        httpClient.get(url: url, parameters: parameters).responseJSON { response in
            self.handleResponse(response: response, success: success, failure: failure)
        }
    }
    
    func handleResponse(response: DataResponse<Any>, success: (Int, Any) -> Void,
                        failure: (Int, Any) -> Void ) {
        let statusCode: Int! = response.response?.statusCode
        
        switch response.result {
        case .success :
            if response.result.value != nil {
                success(statusCode, response.data ?? "")
                return
            }
            failure(statusCode, response)
            
        case .failure :
            if let statusCode = response.response?.statusCode {
                failure(statusCode, response.error!.localizedDescription)
                return
            }
            failure(400, response.error!.localizedDescription)
            
        }
    }
}
