//
//  HttpClient.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 22/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import Foundation
import Alamofire

class HTTPClient {
    var manager: SessionManager?
    
    init() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 3
        configuration.timeoutIntervalForResource = 60
        manager = Alamofire.SessionManager(configuration: configuration)
    }
    
    func get(url: URLConvertible, parameters: Parameters?) -> DataRequest {
        return request(url: url, method: .get, parameters: parameters)
    }
    private func request(url: URLConvertible, method: HTTPMethod, parameters: Parameters?) -> DataRequest {
        
        return manager!.request(url, method: method, parameters: parameters,
                                encoding: JSONEncoding.default, headers: self.headers())
            .validate(statusCode: 200..<300)
            .validate(contentType: ["application/json"])
    }
    func headers() -> HTTPHeaders {
        let headers = [
            "Accept": "application/json"
        ]
        return headers
    }
    
}
