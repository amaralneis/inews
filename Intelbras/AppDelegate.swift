//
//  AppDelegate.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 21/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions
        launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        return true
    }

}
