//
//  NewsViewController.swift
//  Intelbras
//  Created by Filipe Amaral Neis on 21/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import UIKit
protocol NewsViewControllerDelegate: class {
    func didReadRow( indexPath: IndexPath)
}

class NewsViewController: UIViewController {
    
    // MARK: - Outlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var readButton: UIButton!
    @IBOutlet weak var unreadButton: UIButton!
    @IBOutlet weak var sortButton: UIBarButtonItem!
    // MARK: - Properties
    var identifier = "newsTableViewCell"
    var itens: [News] = []
    var refresher: UIRefreshControl!
    let activityIndicator = UIActivityIndicatorView(style: .whiteLarge)
    let service = NewsService()
    
    // MARK: — View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupEditButton()
        startActivityIndicator()
        setupTableView()
        setupDelegateDataSource()
        registerCell()
        getNews()
        
    }
    
    override func setEditing(_ editing: Bool, animated: Bool) {
        super.setEditing(editing, animated: animated)
        sortButton.isEnabled = !editing
        setupDisabledInEnabledButton(isEnabled: !itens.isEmpty)
        tableView.allowsMultipleSelectionDuringEditing = true
        tableView.setEditing(editing, animated: false)
        UIView.animate(withDuration: 1) {
            self.stackView.isHidden = !editing
        }
    }
    
    // MARK: - Methods
    func setupDelegateDataSource() {
        tableView.delegate = self
        tableView.dataSource = self
    }
    
    func setupEditButton() {
        self.navigationItem.leftBarButtonItem = self.editButtonItem
    }
    
    func registerCell() {
        let nib = UINib(nibName: String.init(describing: NewsTableViewCell.self), bundle: nil)
        tableView.register(nib, forCellReuseIdentifier: identifier)
    }
    func setupTableView() {
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = .gray
        self.refresher?.addTarget(self, action: #selector(self.refresh(sender:)), for: UIControl.Event.valueChanged)
        self.tableView?.addSubview(refresher!)
        self.tableView.backgroundColor = UIColor.groupTableViewBackground
        self.tableView.tableFooterView = UIView()
    }
    
    func startActivityIndicator() {
        activityIndicator.color = .gray
        view.addSubview(activityIndicator)
        activityIndicator.frame = view.bounds
        activityIndicator.startAnimating()
    }
    
    func setupDisabledInEnabledButton(isEnabled: Bool = false) {
        readButton.isEnabled = isEnabled
        unreadButton.isEnabled = isEnabled
    }
    
    func getNews(isRefresher: Bool =  false) {
        
        service.fetch { [weak self] (response, isSucesso) in
            self?.dimmissLoading(isRefresher: isRefresher)
            if isSucesso {
                guard let itens  = response as? [News] else {
                    self?.alertErro(msg: response as? String ?? "")
                    return
                }
                self?.itens = itens
                self?.tableView.reloadData()
                return
            }
            self?.alertErro(msg: response as? String ?? "")
        }        
    }
    
    func dimmissLoading(isRefresher: Bool) {
        if isRefresher {
            self.refresher.endRefreshing()
        } else {
            self.activityIndicator.removeFromSuperview()
        }
    }
    
    func actionSheet() {
        let message = "Select a typeof filter"
        let alert = UIAlertController(title: "Sort", message: message, preferredStyle: .actionSheet)
        let dataAction = UIAlertAction(title: "Data", style: .default) { (_) in
            self.sortDate()
        }
        let titleAction = UIAlertAction(title: "Title", style: .default) { (_) in
            self.sortTitle()
        }
        let authorAction = UIAlertAction(title: "Author", style: .default) { (_) in
            self.sortAuthor()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alert.addAction(dataAction)
        alert.addAction(titleAction)
        alert.addAction(authorAction)
        alert.addAction(cancelAction)
        
        if let popoverController = alert.popoverPresentationController {
            popoverController.barButtonItem = sortButton
        }
        
        self.present(alert, animated: true, completion: nil)
    }
    
    // MARK: - Sort
    func sortDate() {
        self.itens = self.itens.sorted(by: {
            $0.date ?? "" < $1.date ?? ""
        })
        tableView.reloadData()
    }
    
    func sortAuthor() {
        self.itens = self.itens.sorted(by: {
            $0.authors ?? "" < $1.authors ?? ""
        })
        tableView.reloadData()
    }
    
    func sortTitle() {
        self.itens = self.itens.sorted(by: {
            $0.title ?? "" < $1.title ?? ""
        })
        tableView.reloadData()
    }
    
    // MARK: - Actions
    @objc func refresh(sender: AnyObject) {
        self.itens = []
        self.getNews(isRefresher: true)
    }
    
    @IBAction func sortsAction(_ sender: Any) {
        actionSheet()
    }
    @IBAction func readAction(_ sender: Any) {
        
        for indexPath in (self.tableView?.indexPathsForSelectedRows)! {
            let cell = tableView.cellForRow(at: indexPath) as? NewsTableViewCell
            cell?.setupCellRead()
        }
        
        self.setEditing(false, animated: true)
        
    }
    @IBAction func unreadAction(_ sender: Any) {
        
        for indexPath in (self.tableView?.indexPathsForSelectedRows)! {
            let cell = tableView.cellForRow(at: indexPath) as? NewsTableViewCell
            cell?.setupCellUnread()
        }
        
        self.setEditing(false, animated: true)
        
    }
}

extension NewsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if tableView.isEditing {
            return
        }
        
        let viewModel  = NewsViewModel(itens[indexPath.row])
        let controller = NewsDetailsViewController.create(newsViewModel: viewModel)
        controller.delegate = self
        controller.indexPath = indexPath
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

extension NewsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return itens.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: identifier, for: indexPath)
        if itens.isEmpty {
            return cell
        }
        
        if let cell = cell as? NewsTableViewCell {
            cell.setupCell(news: itens[indexPath.row])
            return cell
        }
        
        return cell
    }
}

extension NewsViewController: NewsViewControllerDelegate {
    func didReadRow(indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as? NewsTableViewCell
        cell?.setupCellRead()
        tableView.deselectRow(at: indexPath, animated: false)
    }
}
