//
//  NewsDetailsViewController.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 21/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import UIKit
import Kingfisher

class NewsDetailsViewController: UIViewController {
    
    // MARK: - Outlet
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorsLabel: UILabel!
    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var websiteLabel: UILabel!
    @IBOutlet weak var tagsLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    
    // MARK: - Propierts
    var newsViewModel: NewsViewModel?
    weak var delegate: NewsViewControllerDelegate!
    var indexPath: IndexPath!
    
    // MARK: - Life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    override func viewWillAppear(_ animated: Bool) {
        update()
    }
    // MARK: - Create
    static func create(newsViewModel: NewsViewModel) -> NewsDetailsViewController {
        let identifier = String.init(describing: NewsDetailsViewController.self)
        let viewController = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: identifier) as! NewsDetailsViewController
        viewController.newsViewModel = newsViewModel
        return viewController
    }
    
    // MARK: - Methods
    private func update() {
        
        guard let viewModel = newsViewModel else { return }
        navigationItem.title = "Details"
        titleLabel.text = viewModel.title
        contentLabel.text = viewModel.content
        websiteLabel.text = viewModel.website
        authorsLabel.text = viewModel.authors
        dateLabel.text = viewModel.date
        tagsLabel.text = viewModel.tags
        avatarImageView.round()
        delegate.didReadRow(indexPath: indexPath)
        
        if !viewModel.imageUrl.isEmpty {
            avatarImageView.kf.setImage(with: URL(string: viewModel.imageUrl))
        }
    }
    
    private func setTextColor() {
        titleLabel.textColor = .customGray
        contentLabel.textColor = .customGray
        websiteLabel.textColor = .customGray
        authorsLabel.textColor = .customGray
        dateLabel.textColor = .customGray
        tagsLabel.textColor = .customGray
    }
}
