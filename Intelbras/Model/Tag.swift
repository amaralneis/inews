//
//  Tag.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 21/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import Foundation

struct Tag: Codable {
    var idTag: Int?
    var label: String?
    
    private enum CodingKeys: String, CodingKey {
        case idTag = "id", label
    }
}
