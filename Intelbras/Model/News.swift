//
//  News.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 21/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import Foundation

protocol NewsProtocol {
    var authors: String { get }
    var content: String { get }
    var date: String { get }
    var imageUrl: String { get }
    var title: String { get }
    var website: String { get }
    var tags: String { get }
}

struct News: Codable {
    var authors: String?
    var content: String?
    var date: String?
    var imageUrl: String?
    var title: String?
    var website: String?
    var tags: [Tag]?
}
