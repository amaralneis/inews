//
//  NewsViewModel.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 21/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import Foundation

class NewsViewModel: NewsProtocol {
    let news: News
    
    init(_ news: News) {
        self.news = news
    }
    
    var authors: String {
        return news.authors ?? ""
    }
    
    var content: String {
        return news.content ?? ""
    }
    
    var date: String {
        return news.date ?? ""
    }
    
    var imageUrl: String {
        return news.imageUrl ?? ""
    }
    
    var title: String {
        return news.title ?? ""
    }
    
    var website: String {
        return news.website ?? ""
    }
    
    var tags: String {
        guard let tags = news.tags else { return "" }
        var text = ""
        for tag in tags {
            text.append(" ")
            text.append(tag.label ?? "")
            text.append(",")
        }
        
        text.removeLast()
        return text
    }
}
