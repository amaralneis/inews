//
//  NewsTableViewCell.swift
//  Intelbras
//authors
//  Created by Filipe Amaral Neis on 21/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import UIKit
import Kingfisher
class NewsTableViewCell: UITableViewCell {

    // MARK: - Outlet
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!

    // MARK: - Setup cell
    override func awakeFromNib() {
        super.awakeFromNib()
        
        avatarImageView.round()
        dateLabel.textColor = .customGray
        authorLabel.textColor = .customGray
        titleLabel.textColor = .customGray
    }
    
    func setupCell(news: News) {
        let newsViewModel = NewsViewModel(news)
        authorLabel.text = newsViewModel.authors
        titleLabel.text = newsViewModel.title
        dateLabel.text = newsViewModel.date
        
        if !newsViewModel.imageUrl.isEmpty {
           avatarImageView.kf.setImage(with: URL(string: newsViewModel.imageUrl))
        }
    }
    func setupCellRead() {
        titleLabel.textColor = .customGray
        titleLabel.font = UIFont.systemFont(ofSize: 17)
    }
    func setupCellUnread() {
        titleLabel.textColor = .black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 17)
    }

}
