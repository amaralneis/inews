//
//  UIColor+.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 24/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import UIKit

extension UIColor {    
    class var  customGray: UIColor {
        return self.init(red: 0.55, green: 0.55, blue: 0.55, alpha: 1.0)
    }
}
