//
//  UIView+.swift
//  Intelbras
//
//  Created by Filipe Amaral Neis on 22/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import UIKit

extension UIView {
    
    func round() {
        self.layer.cornerRadius = self.frame.width / 2
        self.clipsToBounds = true
    }
}
