//
//  UIViewController+.swift
//  Intelbras
//
//  Created by Filipe Amaral Néis on 23/11/18.
//  Copyright © 2018 Filipe Amaral Neis. All rights reserved.
//

import UIKit

extension UIViewController {
 
    func alertErro(msg: String) {
        let alert = UIAlertController(title: "Error ☹️", message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
}
